<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 11.11.17
 * Time: 17:12
 */

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

class SignupForm extends ActiveRecord
{
    public $password_repeat;
    public $verifyCode;


    public static function tableName()
    {
        return 'signupsuser';
    }

    public function attributeLabels()
    {
        return [
            'login' => 'Login',
            'email' => 'E-mail',
            'password' => 'Password',
            'password_repeat' => 'Confirm Password',
            'verifyCode' => 'Verification Code',
        ];
    }


    public function rules()
    {
        return [
            [['login' , 'email' , 'password' ], 'required'],
            ['email' ,  'email'],
            ['verifyCode', 'captcha'],
            ['verifyCode' , 'required'],
            ['login' ,  'string' , 'length' => [2,16]],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'required'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
            [['login'], 'unique', 'message' => 'Login already exist.'],
            [['email'], 'unique', 'message' => 'Login already exist.'],

        ];
    }

    public function beforeSave($insert) {
        $password = md5($this->password);
        $this->password = $password;
        return true;
    }
}