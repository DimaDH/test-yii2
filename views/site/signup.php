<?php
use app\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

?>


<?php $form = ActiveForm::begin(); ?>

<?= $form->field($signup, 'login')->label('Login') ?>

<?= $form->field($signup, 'email')->label('E-mail')->input('email') ?>

<?= $form->field($signup, 'password')->label('Password')->input('password') ?>

<?= $form->field($signup, 'password_repeat')->label('Confirm Password')->input('password') ?>

<?= $form->field($signup, 'verifyCode')->widget(Captcha::className())->label('Verification Code') ?>

<?= Html::submitButton('Sign Up', ['class' => 'btn btn-primary']) ?>

<?php $form = ActiveForm::end(); ?>