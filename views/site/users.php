<?php if ($users) : ?>
    <table align="center" width="90%" border="1px" id="myTable" class="tablesorter">

        <thead>
        <tr>
            <th>Login</th>
            <th>E-mail</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($users as $user) : ?>
            <tr>
                <td><?php echo $user->login; ?></td>
                <td><?php echo $user->email; ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<div id="pager" class="container">
    <ul class="pagination">
        <li class="first"><a href="#"><<</a></li>
        <li class="prev"><a href="#"><</a></li>
        <li class="next"><a href="#">></a></li>
        <li class="last"><a href="#">>></a></li>
        <li><input class="pagedisplay form-control" style="width: 23%" disabled></li>
    </ul>
    <input class="pagesize" value="2" type="hidden"/>
</div>







