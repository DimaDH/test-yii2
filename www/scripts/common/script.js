jQuery(document).ready(function ($) {
    $("#myTable").tablesorter({
        widgets: ['zebra'],
        debug: true,
        widthFixed: true

    }).tablesorterPager({
        size: 2,
        container: $('.container'),
        positionFixed: false,
        page: 0,
        cssFirst: '.first',
        cssNext: '.next',
        cssPrev: '.prev',
        cssLast: '.last'
    });
});